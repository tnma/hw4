from hw4 import more_functions, guessing_game, speech_parser

print(more_functions.odd_even_merge([1, 2, 3, 4], [1, 2, 3, 4]))
print(more_functions.make_dict([0, 1, 2, 3]))
print(more_functions.transpose_matrix([[11, 12, 13, 14], [21, 22, 23, 24]]))
print(more_functions.merge_dicts({0: 1}, {1: 1}))
print(more_functions.squares(1, 3))
guessing_game.guessing_game()
speech_parser.parse_text('speech.txt')
speech_parser.parse_text('speech.txt', True)
