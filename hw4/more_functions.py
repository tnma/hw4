def odd_even_merge(list1, list2):
    # return the merged list
    return [n for n in list1 if n % 2 == 0] + [n for n in list2 if n % 2 == 1]


def make_dict(a_list):
    return {'key' + str(n): a_list[n] for n in range(len(a_list))}


def transpose_matrix(matrix):
    return zip(*matrix)


def merge_dicts(dict1, dict2):
    merged_dict = {}
    # put values from dict1 to merged_dict
    for k in dict1:
        merged_dict[k] = dict1[k]
    # do the same for dict2
    for k in dict2:
        merged_dict[k] = dict2[k]
    return merged_dict


def squares(start, qty):
    first_root = int(start**.5)
    return [x**2 for x in range(first_root, first_root + qty)]
