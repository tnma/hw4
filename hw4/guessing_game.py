import random


def guessing_game(low=0, high=20):
    mystery_num = random.randrange(low, high)
    guess = None
    while mystery_num != guess:
        guess = int(raw_input('Guess a number between ' + str(low) + ' and ' + str(high) + ': '))
        if mystery_num > guess:
            print('The mystery number is larger')
        elif mystery_num < guess:
            print('The mystery number is smaller')
    print('You won!')
