def parse_text(filename, write_to_file=False, output_filename='out.txt', ignore_file='ignore.txt', top=10):
    # get words to ignore
    ignore_words = get_word_list(ignore_file)
    # get words from file
    words = get_word_list(filename)
    # remove words to ignore
    words = [word for word in words if not ignore_words.__contains__(word)]
    # get word counts
    word_count = get_word_count_dict(words)
    word_count_sorted = sort_words(word_count)
    # check if there are less words than top
    top = len(word_count_sorted) if len(word_count_sorted) < top else top
    # print top words
    output = 'Top ' + str(top) + ' words in \'' + filename + '\' are:' + '\n'
    for n in range(top):
        word = word_count_sorted[n]
        output += (str(n + 1) + '. ' + word + ': ' + str(word_count[word])) + '\n'
    if write_to_file:
        f = open(output_filename, 'w')
        f.write(output)
    else:
        print(output)


def strip_non_alpha(string):
    strip = ''.join(c for c in map(chr, range(256)) if not c.isalnum() and c != ' ' and c != '\n' and c != '\'')
    return string.translate(None, strip)


def get_word_count_dict(words):
    word_count = {word: 0 for word in words}
    for word in words:
        word_count[word] += 1
    return word_count


def sort_words(word_count):
    word_count_sorted = []
    word_count_work = {k: v for k, v, in word_count.iteritems()}
    for _ in range(len(word_count)):
        # get key associated with next highest word count
        next_key = reduce(lambda x, y: x if word_count_work[x] > word_count_work[y] else y, word_count_work.keys())
        word_count_sorted.append(next_key)
        # pop out the key with highest word count
        word_count_work.pop(next_key)
    return word_count_sorted


def get_word_list(filename):
    f = open(filename, 'r')
    contents = f.read()
    f.close()
    contents = strip_non_alpha(contents)
    contents = contents.replace('\n', ' ')
    contents = contents.lower()
    # return list of alphanumeric words
    return [word for word in contents.split(' ') if len(word) != 0]
